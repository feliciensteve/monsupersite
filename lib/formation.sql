-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Client :  localhost
-- Généré le :  Ven 09 Mars 2018 à 18:48
-- Version du serveur :  10.1.21-MariaDB
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `formation`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_for_memberc`
--

CREATE TABLE `t_for_memberc` (
  `FMC_id` int(11) NOT NULL,
  `FMC_login` varchar(100) NOT NULL,
  `FMC_password` varchar(100) NOT NULL,
  `FMC_email` varchar(100) NOT NULL,
  `FMC_fk_FMY` int(11) NOT NULL,
  `FMC_dateadd` varchar(100) NOT NULL,
  `FMC_dateupdate` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_for_memberc`
--

INSERT INTO `t_for_memberc` (`FMC_id`, `FMC_login`, `FMC_password`, `FMC_email`, `FMC_fk_FMY`, `FMC_dateadd`, `FMC_dateupdate`) VALUES
(1, 'steve', 'dilu', 'dilu_steve@yahoo.fr', 3, '2018-03-09 19:35:56', '2018-03-09 19:35:56'),
(2, 'eee', 'eee', 'ee@eee.eee', 3, '2018-03-09 19:46:19', '2018-03-09 19:46:19');

-- --------------------------------------------------------

--
-- Structure de la table `t_for_newsc`
--

CREATE TABLE `t_for_newsc` (
  `FNC_id` int(11) NOT NULL,
  `FNC_title` varchar(100) NOT NULL,
  `FNC_content` varchar(100) NOT NULL,
  `FNC_dateadd` varchar(100) NOT NULL,
  `FNC_dateupdate` varchar(100) NOT NULL,
  `FNC_login` varchar(100) NOT NULL,
  `FNC_fk_FMC` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_for_memberc`
--
ALTER TABLE `t_for_memberc`
  ADD PRIMARY KEY (`FMC_id`);

--
-- Index pour la table `t_for_newsc`
--
ALTER TABLE `t_for_newsc`
  ADD PRIMARY KEY (`FNC_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_for_memberc`
--
ALTER TABLE `t_for_memberc`
  MODIFY `FMC_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `t_for_newsc`
--
ALTER TABLE `t_for_newsc`
  MODIFY `FNC_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
